package org.example.rabbitRpc.client

import com.rabbitmq.client.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import org.example.rabbitRpc.UserProps
import java.nio.charset.Charset
import java.util.*
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.BlockingQueue

class Rpc {
    private val factory = ConnectionFactory()
    init {
        factory.setUri(UserProps.uri)
        factory.connectionTimeout = UserProps.connectionTimeout
    }


    fun callback(message: String): String {
        val connectionUUID: String = generateUUID()
        val connection = createConnection()
        val channel = createChannel(connection)
        val callbackQueueName = createTempQueue(channel)
        val connectionProps = AMQP.BasicProperties.Builder()
            .replyTo(callbackQueueName)
            .correlationId(connectionUUID)
            .build()

        sendRequestAsync(channel, connectionProps, message)
        val ctagXResult = getResponseValue(channel, callbackQueueName, connectionUUID)

        channel.basicCancel(ctagXResult.first)
        connection.close();
        return ctagXResult.second
    }

    private fun sendRequestAsync(channel: Channel, props: AMQP.BasicProperties, message: String) {
        GlobalScope.async { channel.basicPublish("", UserProps.requestQueueName, props, message.toByteArray()) }
    }

    private fun getResponseValue(
        channel: Channel,
        callbackQueueName: String,
        correlationId: String
    ): Pair<String, String> {
        val response: BlockingQueue<String> = ArrayBlockingQueue(1)
        val onResponseArrival = { consumerTag: String, delivery: Delivery -> onResponseArrival(correlationId, delivery, response) }
        val onResponseFail = { consumerTag: String -> onResponseFailed(consumerTag) }
        val ctag = channel.basicConsume(callbackQueueName, true, onResponseArrival, onResponseFail)
        return Pair(ctag, response.take())
    }

    private fun onResponseArrival(correlationId: String, delivery: Delivery, multithreadResultField: BlockingQueue<String>) {
        println("response delivered to the client for correlation: $correlationId")
        if (hasStrayed(delivery, correlationId))
            multithreadResultField.offer(String(delivery.body, Charset.defaultCharset()))
    }

    private fun onResponseFailed(consumerTag: String) =  println("cancelling callback for: $consumerTag")
    private fun createConnection() = factory.newConnection()
    private fun createChannel(connection: Connection) = connection.createChannel()
    private fun createTempQueue(channel: Channel) = channel.queueDeclare().getQueue()
    private fun generateUUID() = UUID.randomUUID().toString()
    private fun hasStrayed(delivery: Delivery, correlationId: String) = delivery.properties.correlationId == correlationId
}