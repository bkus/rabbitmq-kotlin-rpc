package org.example.rabbitRpc

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import org.example.rabbitRpc.client.Rpc
import org.example.rabbitRpc.server.Server
import java.time.LocalDate


fun main(args: Array<String>) {
    println("starting")
    GlobalScope.async { Server().start {heavyComputing(it)} }

    while (true){
        val computationResult = Rpc().callback("data for computation")
        println(computationResult)
        Thread.sleep(30 * 1000)
    }
}

private fun heavyComputing(arg: String): String{
    println("3rd party service received a message: $arg")
    val now = LocalDate.now()
    return "$arg: $now"
}