package org.example.rabbitRpc

import com.rabbitmq.client.AMQP

object UserProps {
    val requestQueueName = System.getenv("QUEUE_NAME") ?: "request_rpc"
    val uri = System.getenv("CLOUDAMQP_URL") ?: "amqp://cloudamqp.com/" // <------ Probably you want to change it
    val connectionTimeout = Integer.parseInt(System.getenv("CONNECTION_TIMEOUT") ?: "30000")
}
