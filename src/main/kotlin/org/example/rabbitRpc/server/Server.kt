package org.example.rabbitRpc.server

import com.rabbitmq.client.*
import java.nio.charset.Charset
import org.example.rabbitRpc.UserProps

class Server {
    private val factory = ConnectionFactory()
    init {
        factory.setUri(UserProps.uri)
        factory.connectionTimeout = UserProps.connectionTimeout
    }

    fun start(computatingFunction: (String) -> String) {
        val connection = createConnection()
        val channel = createChannel(connection)

        try {
            setupChannel(channel)
            println(" [x] Awaiting RPC requests")
            val monitor = Object()
            val deliverCallback =
                DeliverCallback { consumerTag: String, delivery: Delivery -> sendCallback(monitor, channel, consumerTag, delivery, computatingFunction) }
            val cancelCallback = CancelCallback {}
            startConsumerAsync(channel, deliverCallback, cancelCallback)
            fancyBlockingFunction(monitor)
        } finally {
            channel.close()
            connection.close()
        }
    }


    private fun createConnection() = factory.newConnection()
    private fun createChannel(connection: Connection) = connection.createChannel()
    private fun setupChannel(channel: Channel){
        channel.queueDeclare(UserProps.requestQueueName, false, false, false, null)
        channel.queuePurge(UserProps.requestQueueName)
        channel.basicQos(1)
    }
    private fun startConsumerAsync(channel: Channel, callback: DeliverCallback, cancelCallback: CancelCallback) =
        channel.basicConsume(UserProps.requestQueueName, false, callback, cancelCallback)
    private fun fancyBlockingFunction(monitor: Object) {
        // Wait and be prepared to consume the message from RPC client.
        while (true) {
            synchronized(monitor) {
                try {
                    monitor.wait()
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun sendCallback(monitor: Object, channel: Channel, consumerTag: String, delivery: Delivery, computingFunction: (String) -> String){
        val replyProps = AMQP.BasicProperties.Builder()
            .correlationId(delivery.properties.correlationId)
            .build()
        var response = ""
        try {
            response = computingFunction(String(delivery.body, Charset.defaultCharset()))
        } catch (e: RuntimeException) {
            println("$e")
        } finally {
            channel.basicPublish(
                "",
                delivery.properties.replyTo,
                replyProps,
                response.toByteArray(Charset.defaultCharset())
            )
            channel.basicAck(delivery.envelope.deliveryTag, false)
            // RabbitMq consumer worker thread notifies the RPC server owner thread
            synchronized(monitor) { monitor.notify() }
        }
    }
}